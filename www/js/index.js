/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};

var items = [{ id: 1, text: 'Feijão', unit: 'quilo(s)' }, { id: 2, text: 'Macarrão', unit: 'pct(s)' }, { id: 3, text: 'Gás', unit: 'unid(s)' }, { id: 4, text: 'Farinha', unit: 'quilo(s)' }, { id: 5, text: 'Batata', unit: 'unid(s)' }];
var options = [{ id: 6, text: 'Arroz', unit: 'quilo(s)' }, { id: 7, text: 'Tomate', unit: 'unid(s)' }, { id: 8, text: 'Biscoito', unit: 'unid(s)' }, { id: 9, text: 'Cebola', unit: 'unid(s)' }, { id: 5, text: 'Soja', unit: 'pct(s)' }];

function add_list_item(attrs){
  $("#items").loadTemplate("templates/list_item.html", {
    id: "item-"+attrs.id,
    text: attrs.text,
    unit: attrs.unit,
    quantity: 0, //TODO
    item: attrs.id
  }, {append: true});
}

$.each(items, function( index, value ) {
  add_list_item(value);
});

$("#add-new-item").select2({
  data: options,
  placeholder: "Adicionar novo item...",
  tags: true,
});

$("#add-new-item").on('select2:select', function(e){
  add_list_item(e.params.data);
  $('#add-new-item option[value='+ e.params.data.id +']').remove();
  $('.select2-selection__rendered').attr('title', null);
  $('.select2-selection__rendered').loadTemplate("templates/placeholder.html");
});

$(document).on('click', '.btn-danger', function(){
  var quantity = jQuery('#item-' + jQuery(this).data('item') + ' input');
  quantity.val(parseInt(quantity.val()) - 1);
  if(parseInt(quantity.val()) < 0)
    quantity.val('0');
});

$(document).on('click', '.btn-success', function(){
  var quantity = jQuery('#item-' + jQuery(this).data('item') + ' input');
  quantity.val(parseInt(quantity.val()) + 1);
});

$(document).on('click', '.actions .remove', function(){
  var item = jQuery('#item-' + jQuery(this).data('item'));
  item.remove();
  return false;
});
